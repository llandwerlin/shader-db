[require]
GLSL >= 4.5

[fragment shader]
#version 450

#define lightCount 6

layout (location = 0) in vec3 inNormal;
layout (location = 2) in vec3 inColor;

layout (location = 3) in vec4 inLightVec[lightCount];

layout (location = 0) out vec4 outFragColor;

#define MAX_LIGHT_DIST 9.0 * 9.0

void main() 
{
	vec3 lightColor[lightCount];
	lightColor[0] = vec3(1.0, 0.0, 0.0);
	lightColor[1] = vec3(0.0, 1.0, 0.0);
	lightColor[2] = vec3(0.0, 0.0, 1.0);
	lightColor[3] = vec3(1.0, 0.0, 1.0);
	lightColor[4] = vec3(0.0, 1.0, 1.0);
	lightColor[5] = vec3(1.0, 1.0, 0.0);
	
	vec3 diffuse = vec3(0.0);
	// Just some very basic attenuation
	for (int i = 0; i < lightCount; ++i)
	{				
		float lRadius =  MAX_LIGHT_DIST * inLightVec[i].w;
	
		float dist = min(dot(inLightVec[i], inLightVec[i]), lRadius) / lRadius;
		float distFactor = 1.0 - dist;		
	
		diffuse += lightColor[i] * distFactor;		
	}
			
	outFragColor.rgb = diffuse;			
}

[vertex shader]
#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 3) in vec3 inColor;

#define lightCount 6

layout (binding = 0) uniform UBO 
{
	mat4 projection;
	mat4 model;
	vec4 lightColor[lightCount];
} ubo;

layout(push_constant) uniform PushConsts {
	vec4 lightPos[lightCount];
} pushConsts;

layout (location = 0) out vec3 outNormal;
layout (location = 2) out vec3 outColor;

layout (location = 3) out vec4 outLightVec[lightCount];

out gl_PerVertex
{
	vec4 gl_Position;
};

void main() 
{
	outNormal = inNormal;
	outColor = inColor;
	
	gl_Position = ubo.projection * ubo.model * vec4(inPos.xyz, 1.0);
	
	for (int i = 0; i < lightCount; ++i)
	{	
		vec4 worldPos =  ubo.model * vec4(inPos.xyz, 1.0);
		outLightVec[i].xyz = pushConsts.lightPos[i].xyz - inPos.xyz;		
		// Store light radius in w
		outLightVec[i].w = pushConsts.lightPos[i].w;
	}
}

