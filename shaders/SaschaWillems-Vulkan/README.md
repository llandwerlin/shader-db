Imported from https://github.com/SaschaWillems/Vulkan at the following commit :

commit d782d4c15e9ac26fc3aecd452d900b38d93eae99
Author: Sascha Willems <webmaster@saschawillems.de>
Date:   Sat Dec 8 18:50:25 2018 +0100

    Added multiview and inlineuniformblocks sample to android build

In this directory, run the following command :

./import_shaders.py ~/path/to/SaschaWillems-Vulkan/data/shaders/
