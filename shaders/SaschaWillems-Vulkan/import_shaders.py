#!/usr/bin/python

import os
import shutil
import sys

walk_dir = os.path.abspath(sys.argv[1]) + '/'

print('Importing shaders from: ' + walk_dir)

stages = ( "frag", "tese", "tesc", "geom", "vert", "comp" )

test_headers = {
    "frag": "[fragment shader]",
    "vert": "[vertex shader]",
    "tese": "[tessellation evaluation shader]",
    "tesc": "[tessellation control shader]",
    "geom": "[geometry shader]",
    "comp": "[compute shader]",
}


shaders = {}
for root, subdirs, files in os.walk(walk_dir):
    if len(files) < 2:
        continue
    path = root[len(walk_dir):]
    for f in files:
        name = os.path.splitext(f)[0]

        for s in stages:
            if f.endswith('.' + s):
                if name not in shaders:
                    shaders[name] = { "dir": path, "name": name }
                shaders[name][s] = os.path.join(root, f)

for shader_name in shaders:
    shader = shaders[shader_name]
    print(shader)
    try:
        os.makedirs(shader["dir"])
    except:
        pass
    f = open(os.path.join(shader["dir"], shader["name"] + ".shader_test"), "w")
    f.write("[require]\n")
    f.write("GLSL >= 4.5\n")
    f.write("\n")
    for stage in stages:
        if stage not in shader:
            continue
        content = open(shader[stage], "r").read()
        f.write(test_headers[stage] + "\n")
        f.write(content)
        f.write("\n\n")
    f.close()
