[require]
GLSL >= 4.5

[fragment shader]
#version 450

layout (binding = 1) uniform sampler2D samplerGradientRamp;

layout (location = 0) in vec3 inColor;
layout (location = 1) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

void main() 
{
	// Use max. color channel value to detect bright glow emitters
	if ((inColor.r >= 0.9) || (inColor.g >= 0.9) || (inColor.b >= 0.9)) 
	{
		outFragColor.rgb = texture(samplerGradientRamp, inUV).rgb;
	}
	else
	{
		outFragColor.rgb = inColor;
	}
}

[vertex shader]
#version 450

layout (location = 0) in vec3 inPos;
layout (location = 2) in vec3 inColor;

layout (binding = 0) uniform UBO 
{
	mat4 projection;
	mat4 model;
	float gradientPos;
} ubo;

layout (location = 0) out vec3 outColor;
layout (location = 1) out vec2 outUV;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main() 
{
	outColor = inColor;
	outUV = vec2(ubo.gradientPos, 0.0f);
	gl_Position = ubo.projection * ubo.model * vec4(inPos, 1.0);
}


